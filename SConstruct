#!/usr/bin/env python
import re
import os
import sys
import glob
import shutil

# The name of the module
name = "godot-window-capture"

# Libraries for windows
win_libs = []

# Add specific variables
# Godot version target
godot_version = ARGUMENTS.get("godot_version", "4.1")
if "godot_version" in ARGUMENTS:
    del ARGUMENTS["godot_version"]

# Sanitize godot_version
if not re.match(r"^\d+[.]\d+$", godot_version):
    print("Error: specified `godot_version` must be in the form major.minor such as '4.1'.")
    Exit(1)

godot_target_major, godot_target_minor = godot_version.split('.')

# Ensure godot-cpp repo is cloned at all
os.system(f"git submodule update --init")
os.system(f"cd godot-cpp; git fetch")

# Use that version of godot
godot_cpp_path = os.path.join(os.path.expanduser('~'), '.cache', f"godot-cpp-{godot_version}")
if not os.path.exists(godot_cpp_path):
    os.system(f"git clone godot-cpp {godot_cpp_path} --shared")
if not os.path.exists(f"godot-cpp-{godot_version}"):
    os.symlink(godot_cpp_path, f"godot-cpp-{godot_version}", target_is_directory=True)
os.system(f"cd {godot_cpp_path}; git checkout godot-{godot_version}-stable")

# Import godot-cpp scons
env = SConscript(f"{godot_cpp_path}/SConstruct")

# Whether or not this is a debug build
debug = env.get('debug_symbols', False)

debug_suffix = ''
if debug:
    debug_suffix = '-dbg'

# Organize a set of flags
flags = []

# The targets we are building
targets = []

# Where we place our own downloaded components
tmp_path = os.path.join('.', 'tmp')

# For reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# tweak this if you want to use different folders, or more folders, to store your source code in.
flags.append(f"-Isrc")
sources = Glob("src/*.cpp")

env.Append(CCFLAGS=["-fcoroutines"])

# Add ffmpeg include path
flags.append(f"-Ithird-party/{env['platform']}{debug_suffix}/{env['arch']}/include")

# Negotiate bitness
env.Append(TARGET_ARCH='i386' if env['platform'].endswith('32') else 'x86_64')

# Add ffmpeg link options
output_path = '#bin/' + env['platform'] + '/'

# Determine where our dependencies are built and installed to
lib_prefix = os.path.join('third-party', f"{env['platform']}{debug_suffix}", env['arch'])
lib_path = lib_prefix + '/lib'

winrt_path = lib_prefix

# Copy over winrt files as needed
if env['platform'] == 'windows':
  if not os.path.exists(os.path.join(lib_prefix, "include", "winrt")):
    #shutil.copytree("./external/winrt", os.path.join(lib_prefix, "include", "winrt"))
    pass

# Check for dependencies
if env['platform'] == 'windows':
  winrt_path = os.environ.get('WINRT_PATH', winrt_path)
  if not os.path.exists(os.path.join(winrt_path, 'include', 'winrt', 'base.h')):
      #print("Need winrt to exist within WINRT_PATH: " + winrt_path)
      #Exit(1)
      pass

# Add libraries via pkgconfig
libfilter = "*.so"
if env['platform'] == 'windows':
  libfilter = "*.dll.a"

for libname in glob.glob(os.path.join(lib_path, libfilter)):
    libname = os.path.splitext(os.path.basename(libname))[0]
    if '.dll' in libname:
        libname = os.path.splitext(os.path.basename(libname))[0]
    env.ParseConfig(f"PKG_CONFIG_PATH={lib_path}/pkgconfig pkg-config {libname} --libs")

if env['platform'] == 'windows':
    # Windows needs to link to the dlls
    lib_path = os.path.join(lib_path, '..', 'bin')

    for flag in [f"-l{libname}{lib_ext}" for libname in win_libs]:
        flags.append(flag)

# Add FFmpeg lib path
flags.append(f"-L{lib_path}")

# Add our own lib_path
flags.append(f"-L{output_path}")

# Create gdextension file from template
gdextension = env.Command(target=f'dist/{godot_version}{debug_suffix}/{name}/{name}.gdextension',
                          source='./module.gdextension',
                          action="sed -e 's/gdexample/" + name + f"/g' -e 's/x[.]x/{godot_version}/g' < $SOURCE > $TARGET")

# Add any extra files
if env["platform"] == "windows":
    for libname in glob.glob(os.path.join(lib_path, '*.dll')):
        libname = os.path.basename(libname)
        targets.append(
            env.Command(
                f'dist/{godot_version}{debug_suffix}/{name}/{env["platform"]}/{env["arch"]}/{libname}',
                f'./third-party/{env["platform"]}{debug_suffix}/{env["arch"]}/bin/{libname}',
                Copy('$TARGET', '$SOURCE')
            )
        )
else:
    for libname in glob.glob(os.path.join(lib_path, '*.so*')):
        libname = os.path.basename(libname)
        targets.append(
            env.Command(
                f'dist/{godot_version}{debug_suffix}/{name}/{env["platform"]}/{env["arch"]}/{libname}',
                f'./third-party/{env["platform"]}{debug_suffix}/{env["arch"]}/lib/{libname}',
                Copy('$TARGET', '$SOURCE')
            )
        )

# Determine the library name for our output
if env["platform"] == "macos":
    library = env.SharedLibrary(
        "dist/{}/{}/osx/bin/lib{}.{}.{}.framework/lib{}.{}.{}".format(
            f"{godot_version}{debug_suffix}", name,
            name, env["platform"], env["target"],
            name, env["platform"], env["target"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )
else:
    library = env.SharedLibrary(
        "dist/{}/{}/{}/{}/lib{}{}{}".format(
            f"{godot_version}{debug_suffix}",
            name,
            env["platform"],
            env["arch"],
            name, env["suffix"], env["SHLIBSUFFIX"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )

# Tar/Zip distributions
targets.append(
    env.Zip(f"dist/{name}_godot-{godot_version}{debug_suffix}", [f"dist/{godot_version}{debug_suffix}"], ZIPROOT=f"dist/{godot_version}{debug_suffix}")
)

Default(library, gdextension, *targets)
