#include "window_sprite_2d.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;

extern "C" {
HRESULT __stdcall CreateDirect3D11DeviceFromDXGIDevice(
	::IDXGIDevice *dxgiDevice, ::IInspectable **graphicsDevice);

HRESULT __stdcall CreateDirect3D11SurfaceFromDXGISurface(
	::IDXGISurface *dgxiSurface, ::IInspectable **graphicsSurface);
}

struct __declspec(uuid("A9B3D012-3DF2-4EE3-B8D1-8695F457D3C1"))
	IDirect3DDxgiInterfaceAccess : ::IUnknown {
	virtual HRESULT __stdcall GetInterface(GUID const &id,
					       void **object) = 0;
};

extern "C" BOOL winrt_capture_supported()
try {
	/* no contract for IGraphicsCaptureItemInterop, verify 10.0.18362.0 */
	return winrt::Windows::Foundation::Metadata::ApiInformation::
		IsApiContractPresent(L"Windows.Foundation.UniversalApiContract",
				     8);
} catch (const winrt::hresult_error &err) {
	blog(LOG_ERROR, "winrt_capture_supported (0x%08X): %s",
	     err.code().value, winrt::to_string(err.message()).c_str());
	return false;
} catch (...) {
	blog(LOG_ERROR, "winrt_capture_supported (0x%08X)",
	     winrt::to_hresult().value);
	return false;
}

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void WindowSprite2D::_bind_methods() {
	// get_size
	ClassDB::bind_method(D_METHOD("get_size"), &WindowSprite2D::get_size);

	// get_texture
	ClassDB::bind_method(D_METHOD("get_video_texture"), &WindowSprite2D::get_video_texture);
}

/**
 * Construct and initialize the class.
 */
WindowSprite2D::WindowSprite2D() {
	this->_prepare_decoder();
}

/**
 * Create resources.
 */
bool WindowSprite2D::_prepare_decoder() {
	// Success.
	return true;
}

/**
 * Clean up.
 */
WindowSprite2D::~WindowSprite2D() {
	//this->_worker.join();
}

/**
 * Returns the dimensions of the camera.
 */
Vector2 WindowSprite2D::get_size() const {
	return Vector2(this->_width, this->_height);
}

/**
 * Returns a reference to the internal texture.
 */
Ref<Texture2D> WindowSprite2D::get_video_texture() const {
	return this->_texture;
}
