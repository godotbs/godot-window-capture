#ifndef GD_WINDOW_SPRITE_2D_HPP
#define GD_WINDOW_SPRITE_2D_HPP

// Import standard library functions
#include <thread>
#include <chrono>

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/sprite2d.hpp>

// Import support classes
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/templates/vector.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/image_texture.hpp>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <dxgi.h>
#include <inspectable.h>
#include <winrt/Windows.Foundation.h>

// Import OpenCV
//#include <opencv2/videoio/videoio.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

namespace godot {
	class WindowSprite2D : public Sprite2D {
		GDCLASS(WindowSprite2D, Sprite2D)

		private:
			// Our internal texture
			Ref<ImageTexture> _texture;

			// Internal Godot frame data
			PackedByteArray _frame_data;

			// Size of data frame in pixels
			int _width;
			int _height;

			// Internal counter
			float _elapsed;

			// Frames-per-second
			float _fps;
			float _calculated_fps;

			// Places raw frame data into our texture
			bool _prepare_decoder();

			// Capture loop worker
			//std::thread _worker;

			// And consumer thread entrypoint
			//void _loop();

		public:
			WindowSprite2D();
			~WindowSprite2D();

			// Frame processing
			//void _process(float delta);

			// Returns the size of the camera texture
			Vector2 get_size() const;

			// Returns the internal video texture
			Ref<Texture2D> get_video_texture() const;

			// Returns the video frame as an image
			//Ref<ImageTexture> get_videoframe();

		protected:
			// Registers GDScript methods, etc
			static void _bind_methods();
	};
}

#endif
